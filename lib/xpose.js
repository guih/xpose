var request = require('request')
,	colors = require('colors')
,	async = require('async')
, 	reference = false
,	util = require('util')
,	urlUtil = require('url')
,	cheerio = require('cheerio')
,	ucfirst = require('ucfirst')
,	prompt = require('prompt')
,	args = process.argv
,	cloudscraper = require('cloudscraper')
,	targetDropProtocol = null
,	blackLists = [
		"https://www.google.com/safebrowsing/diagnostic?output=jsonp&site=%s"
	,	"http://safeweb.norton.com/report/show?url=%s"
	,	{
		apiKey: "2ab18d311b40d813877f747c1e39790421b67599192d52d8a9e4e8e73de97781",
		target: "http://checkurl.phishtank.com/checkurl/"
	}
	,	"http://www.siteadvisor.com/sites/%s"
	,	"http://www.spamhaus.org/query/domain/%s"
	,	"http://trafficlight.bitdefender.com/info?url=%s"
	,	"https://www.yandex.com/infected?url=%s&l10n=en&redircnt="
]
,	BlackList = {};

/**
 * Check url if it's on blacklists
 * @param {String} 		url 	url string that should be scanned
 * @param {Function} 	fn  	function that will return data
 */
function _check(target, reference, fn){


 	if( typeof fn == "undefined" )
 		console.error(colors.red.bold('Err: You must give and callback function to get data.'));

	if( typeof target == "undefined" || !(/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/).test(target) ){
 		console.error(colors.red.bold('Err: Not a valid target.'));
 		return;
		
	}


 	target = target;
 	reference = reference;

 	getBanner();
 	console.log(colors.bold('Please wait...\n'));

 	console.error(colors.green.bold('[ + ] Performing analisys with target: ' + target+"\n"));

 	_fetchBlackListsState(target, function(data){
		if( typeof fn == "function" )
	 		fn(data || {});
 	});
 }
/**
 * Make http request to url
 * @param  {String}    	_url	
 * @param  {string}  	method	Method way to request POST, GET, PUT
 * @param  {Object}		kwargs 	Object to be passed to request's options argument
 * @param {Function} 	fn  	function(error, response, body) {}
 */
function _httpFetchContents(_url, method, kwargs, fn){
	if( typeof _url == "undefined" )
		console.error(colors.red.bold('Not valid target: ' + _url));

	var options = {
		uri: _url,
		method: method,
	};

	if(!method)
		options.method = "GET";

	if(kwargs)
		options.form = kwargs;

	request(options, function (error, response, body) {
		if (error) {
			return console.error(colors.red.bold('[ - ] ', error ));
		}
		fn(body);
	});
}

/**
 * Make async calls and fetch contents based on API resposes about target
 * @param {String} 		_target 	url string that should be scanned
 * @param {Function} 	_callback 	function that return raw data about target
 */
function _fetchBlackListsState(_target, _callback){

 	_targetDropProtocol = _target.replace(/^https?:\/\//,'');

	async.series({
	    safeWebGoogle: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[0]
	    	,	url = util.format(service, _target);

	    	_httpFetchContents(url, null, null, function(body){

	    		var extractedJsonp = body.match(/({.*})/g)[0];
	    		var info = ( typeof extractedJsonp == "string" ? JSON.parse(extractedJsonp) : {} );
	    		var websiteInfo = info.website;

	    		eof.service = "google safe web";
	    		if( websiteInfo.malwareListStatus == 'unlisted' &&
	    			websiteInfo.uwsListStatus == 'unlisted' &&
	    			websiteInfo.socialListStatus == 'unlisted' &&
	    			websiteInfo.malwareDownloadListStatus == 'unlisted' &&
	    			websiteInfo.unknownDownloadListStatus == 'unlisted' ){
	    			eof.state = "safe";
	    			eof.isSafe = true;
	    			displayInfo(eof.service, 'plus');

	    		}else{
	     			eof.state = "notSafe";
	    			eof.isSafe = false; 
	    			displayInfo(eof.service, 'minus', url);
	    		}


	    		// console.log(plusSign() + colors.bold( ucfirst(eof.service)+"\n" + plusSign() +"Not listed"));

	    		// return;

	        	fn(null, eof);
	    	});
	    },
	    safeWebNorton: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[1]
			, url = util.format(service, _target);

			_httpFetchContents(url, null, null, function(body){
				
				var $ = cheerio.load(body);
				var caption = body.match(/(Norton Safe Web has analyzed (.*)  for safety and security problems\.)/g)

	    		eof.service = "norton safe web";

				if(  !$('.big_clip').hasClass('icoSafe') ){        		
					eof.state = "notSafe";
					eof.isSafe = false; 
	    			displayInfo(eof.service, 'minus', url);

				}else{
					eof.state = "safe";
					eof.isSafe = true;	
					if( caption )
						eof.caption = caption[0];

					displayInfo(eof.service, 'plus');
				}

				fn(null, eof);
			});
	    },
		phishTank: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[2].target
			,	apiKey = blackLists[2].apiKey
			,	args = {
				url: service,
				format: "json",
				app_key: apiKey
			};

			_httpFetchContents(service, "POST", args, function(body){
				var info = JSON.parse(body) || {};	
				eof.service = "phish Tank";

				if(info.results.in_database !== false){
					eof.state = "notSafe";
					eof.isSafe = false; 
					displayInfo(eof.service, 'minus', url);	
				}else{				
					eof.state = "safe";
					eof.isSafe = true; 
					displayInfo(eof.service, 'plus');
				}
				fn(null, eof);
			});
		},
		siteAdvisor: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[3]
			,	url = util.format(service, _target);

			eof.service = "site Advisor"
			_httpFetchContents(url, null, null, function(body){
				var $ = cheerio.load(body);
				var imageCaption = $('img').attr('src');
				var textCaption = $('.intro').text();

				// var isSuccessImage = imageCaption.indexOf("green-") < 0 ? true : false;
				var isSuccessText = textCaption.indexOf("This link is safe.") < 0 ? false : true;

				if( isSuccessText ){
					eof.state = "safe";
					eof.isSafe = true;
					displayInfo(eof.service, 'plus');
					fn(null, eof);
					return;
				}

				eof.state = "notSafe";
				eof.isSafe = false;
				displayInfo(eof.service, 'minus', url);

				fn(null, eof);
				
			});
		},
		spamHaus: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[4]
			//,	_target = _target
			,	url = util.format(service, urlUtil.parse(_target).hostname);


			cloudscraper.get(url, function(error, response, body) {
				eof.service = "spamHaus";
				
				if (error) {
					console.log(colors.red.bold('[ - ] failed request'));
				} else {

					var isListed = body.toString().match(/(is not listed (.*) DBL)/g);
					var isAlreadyListed = body.toString().match(/(Error 404 - File not found.)/g);

					// console.log(isAlreadyListed)
					// return;
					// // if( isAlreadyListed ){
					// // 	fn(null, {});
					// // }
	
					if( typeof isListed != "null" ){
						eof.state = "safe";
						eof.isSafe = true;	
						eof.caption = isListed[0];	
						displayInfo(eof.service, 'plus');	
					}else{
						eof.state = "notSafe";
						eof.isSafe = false;	
						displayInfo(eof.service, 'minus', url);
					}
					fn(null, eof);					

				}
			});
		},
		bitDefender: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[5]
			,	url = util.format(service, _target);		
		

			_httpFetchContents(url, null, null, function(body){
				var $ = cheerio.load(body);

				var info = $('.container-displayed');
				var isSafe = (info.find('img').attr('src').indexOf('ok_ico') > 0);
				var textCaption = info.find('div').text();

				eof.service = "bitDefender";

				if( isSafe ){
					eof.state = "safe";
					eof.caption = textCaption;
					eof.isSafe = true;
					displayInfo(eof.service, 'plus');				
				}else{
					eof.state = "notSafe";
					eof.isSafe = false;
					displayInfo(eof.service, 'minus', url);
				}
				fn(null, eof);			
			});
		},
		yandex: function(fn){
			var eof = {
				caption: null
			}
			,	service = blackLists[6]
			,	url = util.format(service, _target);		
		

			_httpFetchContents(url, null, null, function(body){
				var $ = cheerio.load(body);
				var info = $('.content__title-sub');

				var isSafe = info.text().indexOf('not infected') > 0;


				eof.service = "yandex";

				if( isSafe ){
					eof.state = "safe";
					eof.caption = info.text();	
					eof.isSafe = true;	
					displayInfo(eof.service, 'plus');			
				}else{
					eof.state = "notSafe";
					eof.isSafe = false;
					displayInfo(eof.service, 'mimus', url);
				}
				fn(null, eof);			
			});
		}
	},
	function(err, results) {
		if(err){
			console.error(colors.red.bold('[ - ]Error: ' + err));
			return;
		}
		
		console.error(colors.bold('\n[ + ] done! '));
		if( typeof _callback == "function" )
			_callback(results);
	});	
}

/*
* Display application banner and usage info
*/
function getBanner(){
 	var banner = [
		'                                                       ',
		                                                       
		'    _/      _/                        _/_/_/           ',
		'     _/  _/    _/_/_/      _/_/    _/          _/_/    ',
		'      _/      _/    _/  _/    _/    _/_/    _/_/_/_/   ',
		'   _/  _/    _/    _/  _/    _/        _/  _/          ',
		'_/      _/  _/_/_/      _/_/    _/_/_/      _/_/_/     ',
		'           _/                                          ',
		'          _/                                           ',
		'                                                       ',
		'          v1.0.0',
		'                                                       ',
		'This scan will check if target website is listed in one of these databases.',
		'                                                       ',
		
		'* Google safe web'.bold,
		'* Norton safe web'.bold,
		'* PhishTank'.bold,
		'* Site Advisor'.bold,
		'* SpamHaus'.bold,
		'* Bitdefender'.bold,
		'* Yandex'.bold,

		'                                                       ',

	].join("\n");
	console.log(banner)
}

/*
* Get target based on user inoput if theres no data it will be asked
* @param {Function} 	fn 	function that return target
*/
function _getTarget(fn){

	if( args.join(" ").indexOf("-r") > 0 ){
		reference = true;	
	}
	if( args.length > 2 ){
		fn({target: args[2], displayRef: reference});
	}else{

		prompt.start();

		prompt.get(['Target'], function (err, result) {
			if( err ){
				console.error(colors.red.bold('\n[ - ] ', err ));
				return;
			}
			if( result.Target == undefined || result.Target == "" ){
				console.error('[ - ] Not valid target.');
				return;
			}
			fn({target: results.Target, displayRef: reference});
		});	
	}
}

/*
* Display plus sign
*/
function plusSign(){
	return colors.bold.cyan('[ + ] ');
}

/*
* Display minus sign
*/
function minusSign(){
	return colors.bold.red('[ - ] ');
}

/*
* Get target based on user inoput if theres no data it will be asked
* @param {String} 	service 	Name of service used while scan
* @param {String} 	icon 		Type of icon(sign) will be used based on state
* @param {red} 		ref 		Should display reference about link
*/
function displayInfo(service, icon, ref){
	var listed = ( icon !== 'plus' ? "Listed" : "Not listed" );

	if( icon == 'plus' ){
		toLogStr = plusSign() + colors.bold( ucfirst(service)+" => " + listed);		
	}else{
		toLogStr = minusSign() + colors.bold( ucfirst(service)+" => " + listed);
	}
	if( ref && reference ){
		toLogStr = toLogStr + "\n Reference: " + ref;
	}
	console.log(toLogStr);
}

BlackList = {
	check: _check,
	httpReq: _httpFetchContents,
	getTarget: _getTarget 

};

module.exports = BlackList;